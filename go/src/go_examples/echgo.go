package main

import "fmt"
import "os"
import "strconv"

func main () {

	//prints one argument per line
	for i := 1; i < len(os.Args); i++ {
		fmt.Println(i, os.Args[i])
	}

	fmt.Println("BREAK")

	//print all args on 1 line
	var (
		s = ""
		sep = " "
	)

	for i := 1; i < len(os.Args); i++ {
		index := strconv.Itoa(i)
		s += index + sep + os.Args[i] + sep
	}
	fmt.Println(s)
}

	
